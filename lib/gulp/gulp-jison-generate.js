'use strict';

const jison = require('jison');
const path = require('path');
const through = require('through2');

function gulpJisonGenerate() {
    return through.obj(function (original, encoding, done) {
        // Pass through if no contents or directory
        if (original.isNull() || original.isDirectory()) {
            this.push(original);
            return done();
        }

        let parser = new jison.Parser(original.contents.toString());
        let source = parser.generate();

        let file = original.clone({ contents: false });
        file.path = path.join(
            path.dirname(file.path),
            path.basename(file.path, '.jison') + '.js');
        file.contents = new Buffer(source);

        this.push(file);

        return done();
    });
}

module.exports = gulpJisonGenerate;