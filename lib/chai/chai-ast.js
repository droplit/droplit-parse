'use strict';

// Modified chai-shallow-deep-almost-equal

module.exports = function (chai) {
    var Assertion = chai.Assertion;

    function astEqual(expect, actual, path) {
        // Null value
        if (expect === null) {
            if (actual !== null)
                throw `Expected to have null but got ${actual} at path ${path}.`;
            return true;
        }

        // Undefined
        if (typeof expect === 'undefined') {
            if (typeof actual !== 'undefined')
                throw `Expected to have undefined but got ${actual} at path ${path}.`;
            return true;
        }

        // Scalar
        if (/boolean|number|string/.test(typeof expect)) {
            if (expect != actual)
                throw `Expected to have ${expect} but got ${actual} at path ${path}.`;
            return true;
        }

        // Dates
        if (expect instanceof Date) {
            if (actual instanceof Date) {
                if (expect.getTime() != actual.getTime())
                    throw(`Expected to have date ${expect.toISOString()} but got ${actual.toISOString()} at path ${path}.`);
            }
            else
                throw(`Expected to have date ${expect.toISOString()} but got ${actual} at path ${path}.`);
        }

        if (actual === null)
            throw `Expected to have an array/object but got null at path ${path}`;

        for (let prop in expect) {
            if (typeof expect[prop] != 'undefined' && typeof expect[prop] === 'function')
                continue;
            if (typeof actual[prop] == 'undefined' && typeof expect[prop] != 'undefined')
                throw `Expected ${prop} field to be defined at path ${path}.`;
            astEqual(expect[prop], actual[prop], path + (path == '/' ? '' : '/') + prop);
        }

        return true;
    }

    Assertion.addMethod('astEqual', function (expect) {
        try {
            astEqual(expect, this._obj, '/');
        }
        catch (msg) {
            this.assert(false, msg, undefined, expect, this._obj);
        }
    });
};