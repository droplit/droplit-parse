'use strict';

const compress = require('compression');
const express = require('express');

const app = express();
const port = process.env.PORT || 3000;

app.use(compress());
app.use(express.static('./client', { index: 'index.htm' }));

app.listen(port, () => {
    console.log(`Express server listening on port ${port}`);
});