'use strict';

function context(spec) {
    let parser = spec.parser;
    let methods = spec.methods || {};
    let ast = parser.parser.ast;

    let binOps = {
        '|': (a, b) => a.resolve() | b.resolve(),
        '^': (a, b) => a.resolve() ^ b.resolve(),
        '&': (a, b) => a.resolve() & b.resolve(),
        '==': (a, b) => a.resolve() == b.resolve(),
        '!=': (a, b) => a.resolve() != b.resolve(),
        '===': (a, b) => a.resolve() === b.resolve(),
        '!==': (a, b) => a.resolve() !== b.resolve(),
        '<': (a, b) => a.resolve() < b.resolve(),
        '>': (a, b) => a.resolve() > b.resolve(),
        '<=': (a, b) => a.resolve() <= b.resolve(),
        '>=': (a, b) => a.resolve() >= b.resolve(),
        'instanceof': (a, b) => a.resolve() instanceof b.resolve(),
        'in': (a, b) => a.resolve() in b.resolve(),
        '<<': (a, b) => a.resolve() << b.resolve(),
        '>>': (a, b) => a.resolve() >> b.resolve(),
        '>>>': (a, b) => a.resolve() >>> b.resolve(),
        '+': (a, b) => a.resolve() + b.resolve(),
        '-': (a, b) => a.resolve() - b.resolve(),
        '*': (a, b) => a.resolve() * b.resolve(),
        '/': (a, b) => a.resolve() / b.resolve(),
        '%': (a, b) => a.resolve() % b.resolve()
    };
    let logOps = {
        '||': (a, b) => a.resolve() || b.resolve(),
        '&&': (a, b) => a.resolve() && b.resolve()
    };
    let unyOps = {
        'void': arg => void arg.resolve(),
        'typeof': arg => typeof arg.resolve(),
        '+': arg => +arg.resolve(),
        '-': arg => -arg.resolve(),
        '~': arg => ~arg.resolve(),
        '!': arg => !arg.resolve()
    };

    ast.BinaryExpressionNode.prototype.resolve = function () {
        if (binOps[this.operator])
            return binOps[this.operator](this.left, this.right);
        return undefined;
    };
    ast.CallExpressionNode.prototype.resolve = function () {
        let callee = this.callee.resolve();
        let ctx = this.callee.object ? this.callee.object.resolve() : null;
        let args = [];
        for (let i = 0; i < this.arguments.length; i++) {
            let arg = this.arguments[i].resolve();
            args.push(arg);
        }

        return callee.apply(ctx, args);
    };
    ast.ConditionalExpressionNode.prototype.resolve = function () {
        return this.test.resolve() ? this.consequent.resolve() : this.alternate.resolve();
    };
    ast.IdentifierNode.prototype.resolve = function () {
        if ({}.hasOwnProperty.call(methods, this.name))
            return methods[this.name];
        return undefined;
    };
    ast.LiteralNode.prototype.resolve = function () {
        return this.value;
    };
    ast.LogicalExpressionNode.prototype.resolve = function () {
        if (logOps[this.operator])
            return logOps[this.operator](this.left, this.right);
        return undefined;
    };
    ast.MemberExpressionNode.prototype.resolve = function () {
        let obj = this.object.resolve();
        if (this.property.type === 'Identifier')
            return obj[this.property.name];

        let prop = this.property.resolve();
        return obj[prop];
    };
    ast.ProgramNode.prototype.resolve = function () {
        if (this.body.resolve)
            return this.body.resolve();
        return undefined;
    };
    ast.UnaryExpressionNode.prototype.resolve = function () {
        if (unyOps[this.operator])
            return unyOps[this.operator](this.argument);
        return undefined;
    };

    return Object.freeze({
        parser: parser
    });
}

module.exports = context;