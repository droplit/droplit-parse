'use strict';

module.exports = function () {
    const client = './client/';

    const config = {
        client: client,
        customjs: [
            './**/*.js',
            '!./node_modules/**/*.js',
            '!./parsers/**/*.js',
            `!${client}**/*.js`,
            '!./**/require.js',
            '!./**/jquery-2.2.2.min.js'
        ],
        index: './src/index.htm',
        js: './src/**/*.js',
        grammars: './grammars/*.jison',
        parsers: './parsers/',
        partials: './src/style/partials',
        sass: './src/style/style.scss',
        scripts: `${client}scripts`,
        style: `${client}style`,
        stylesrc: './src/style/**/*.scss',
        test: './test.js',
        testContext: './context/behavior.context.js',
        testParser: './parsers/behavior.js'
    };

    Object.freeze(config);
    return config;
};