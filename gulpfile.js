'use strict';

const config = require('./gulp.config.js')();
const gulp = require('gulp');

const generate = require('./lib/gulp/gulp-jison-generate.js');
const runSequence = require('run-sequence');
const merge = require('merge-stream');
const $ = require('gulp-load-plugins')({ lazy: true });

const colors = $.util.colors;

/**
 * List the available gulp tasks
 */
gulp.task('help', $.taskListing);
gulp.task('default', ['help']);

/**
 * Declare gulp tasks
 */
gulp.task('build', buildTask);
gulp.task('build-style', buildStyleTask);
gulp.task('clean', cleanTask);
gulp.task('clean-client', cleanClientTask);
gulp.task('clean-parser', cleanParserTask);
gulp.task('code', codeTask);
gulp.task('copy', copyTask);
gulp.task('generate', generateTask);
gulp.task('lint', lintTask);
gulp.task('review', reviewTask);
gulp.task('test', testTask);
gulp.task('watch', watchTask);

/*
 * Auxiliary functions
 */

function log(msg) {
    if (typeof (msg) === 'object') {
        for (let item in msg)
            if (msg.hasOwnProperty(item))
                $.util.log(colors.cyan(msg[item]));
    }
    else
        $.util.log(colors.cyan(msg));
}

/*
 * Task implementations
 */
function buildTask(callback) {
    log('Building client');

    runSequence('clean-client', ['copy', 'build-style'], callback);
}

function buildStyleTask() {
    log('Compile SCSS -> CSS');

    let target = gulp.src(config.sass);
    return target
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe($.sass({
            includePaths: [config.partials]
        }))
        .pipe($.autoprefixer({ browsers: ['last 2 version', '>5%'] }))
        .pipe(gulp.dest(config.style));
}

function cleanTask(callback) {
    log('Cleaning all');

    runSequence(['clean-client', 'clean-parser'], callback);
}

function cleanClientTask() {
    log('Cleaning client');

    let target = gulp.src(config.client, { read: false });
    return target
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe($.clean());
}

function cleanParserTask() {
    log('Cleaning parser');

    let target = gulp.src(config.parsers, { read: false });
    return target
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe($.clean());
}

function codeTask() {
    log('Checking code styling');

    let source = gulp.src(config.customjs);
    return source
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe($.jscs())
        .pipe($.jscs.reporter());
}

function copyTask() {
    log('Copy source to client');

    let source = gulp.src([config.index, config.js])
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe(gulp.dest(config.client));
    let parser = gulp.src([config.testParser])
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe(gulp.dest(config.scripts));
    return merge(source, parser);
}

function generateTask() {
    log('Generating task');

    let source = gulp.src(config.grammars);
    return source
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe(generate())
        .pipe(gulp.dest(config.parsers));
}

function lintTask() {
    log('Lint JS');

    let source = gulp.src(config.customjs);
    return source
        .pipe($.plumber()) // exit gracefully if something fails after this
        .pipe($.jshint())
        .pipe($.jshint.reporter('default'));
}

function reviewTask(callback) {
    log('Reviewing code');

    runSequence('lint', 'code', callback);
}

function testTask() {
    log('Running unit tests against parser');

    return gulp.src(config.test, { read: true })
        .pipe($.mocha({
            reporter: 'spec',
            parser: config.testParser
        }));
}

function watchTask() {
    gulp.watch(config.stylesrc, ['build-style']);
    gulp.watch(['./src/**/*.*', '!./src/style/**/*.scss'], function (obj) {
        if (obj.type === 'changed') {
            gulp.src(obj.path, { base: './src/' })
                .pipe($.plumber()) // exit gracefully if something fails after this
                .pipe(gulp.dest(config.client));
        }
    });
}