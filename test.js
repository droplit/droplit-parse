'use strict';

const chai = require('chai');
const ctx = require('./context/behavior.context.js');
const glob = require('glob');
const fs = require('fs');
const path = require('path');
const gulpConfig = require('./gulp.config.js')();
const chaiAst = require('./lib/chai/chai-ast.js');

chai.use(chaiAst);

const assert = chai.assert;
const expect = chai.expect;

let parser = require(gulpConfig.testParser);
if (!parser)
    return;

ctx({ parser: parser });

let files = glob.sync('./tests/*/test.js');
files.forEach(file => {
    let config = require(file)();
    let dir = path.dirname(file);
    if (!config.enabled)
        return;

    describe(config.name, () => {
        config.tests.forEach(test => {
            let parsed;
            try {
                parsed = parser.parse(test.expression);
                let resolve = parsed.resolve();

                it(test.name, done => {
                    if (typeof test.expected === 'undefined')
                        assert.isUndefined(resolve);
                    else
                        assert.equal(resolve, test.expected);
                    done();
                });

                if (test.showAst)
                    console.log(parsed);
                if (test.ast) {
                    let astPath = path.resolve(path.join(dir, test.ast));
                    try {
                        fs.accessSync(astPath, fs.F_OK);
                    }
                    catch (e) {
                        console.error(`AST file does not exist at path ${astPath}`);
                        return;
                    }
                    let astTest = require(astPath);
                    it (`${test.name} AST`, done => {
                        expect(astTest).astEqual(parsed);
                        done();
                    });
                }
            } catch (ex) {
                it(test.name, done => {
                    assert.doesNotThrow(parser.parse(test.expression));
                    console.error('error', ex);
                    done();
                });
            }
        });
    });
});