/* jshint browser:true, jquery:true */
/* globals parser */
'use strict';

(function () {
    context({
        parser: parser,
        methods: { get: get }
    });
    $('#parse').on('input', function () {
        var result;
        try {
            result = parser.parse(this.value);
            var tree = createTree(result);
            $('#ast').html(tree);
        }
        catch (ex) {
            $('#ast').text('');
            $('#error div').text(ex.message);
            $('#error').show();
            return;
        }
        try {
            if (result) {
                if (result.resolve) {
                    var resolve = result.resolve();
                    $('#results').text(resolve);
                }
                $('#error div').text('');
                $('#error').hide();
            }
        }
        catch (ex) {
            $('#results').text('');
            $('#error div').text(ex.message);
            $('#error').show();
        }
    });

    function get() {
        return Array.prototype.slice.call(arguments)
            .reduce(function(prev, current) {
                return prev + current;
            }, 0);
    }

    function createTree(obj) {
        var root = document.createElement('ul');
        return (function createBranch(obj, root) {
            for (var prop in obj) {
                var node = document.createElement('li');
                var subObj = obj[prop];
                if (typeof subObj === 'function')
                    continue;
                else if (typeof subObj !== 'object')
                    $(node).text(prop + ': ' + subObj);
                else {
                    $(node).append($(document.createElement('div')).text(prop));
                    var listNode = document.createElement('ul');
                    node.appendChild(listNode);
                    createBranch(subObj, listNode);
                }
                root.appendChild(node);
            }
            return root;
        })(obj, root);
    }

    function context(spec) {
        var parser = spec.parser;
        var methods = spec.methods || {};
        var ast = parser.ast;

        var binOps = {
            '|': function (a, b) { return a.resolve() | b.resolve(); },
            '^': function (a, b) { return a.resolve() ^ b.resolve(); },
            '&': function (a, b) { return a.resolve() & b.resolve(); },
            '==': function (a, b) { return a.resolve() == b.resolve(); },
            '!=': function (a, b) { return a.resolve() != b.resolve(); },
            '===': function (a, b) { return a.resolve() === b.resolve(); },
            '!==': function (a, b) { return a.resolve() !== b.resolve(); },
            '<': function (a, b) { return a.resolve() < b.resolve(); },
            '>': function (a, b) { return a.resolve() > b.resolve(); },
            '<=': function (a, b) { return a.resolve() <= b.resolve(); },
            '>=': function (a, b) { return a.resolve() >= b.resolve(); },
            'instanceof': function (a, b) { return a.resolve() instanceof b.resolve(); },
            'in': function (a, b) { return a.resolve() in b.resolve(); },
            '<<': function (a, b) { return a.resolve() << b.resolve(); },
            '>>': function (a, b) { return a.resolve() >> b.resolve(); },
            '>>>': function (a, b) { return a.resolve() >>> b.resolve(); },
            '+': function (a, b) { return a.resolve() + b.resolve(); },
            '-': function (a, b) { return a.resolve() - b.resolve(); },
            '*': function (a, b) { return a.resolve() * b.resolve(); },
            '/': function (a, b) { return a.resolve() / b.resolve(); },
            '%': function (a, b) { return a.resolve() % b.resolve(); }
        };
        var logOps = {
            '||': function (a, b) { return a.resolve() || b.resolve(); },
            '&&': function (a, b) { return a.resolve() && b.resolve(); }
        };
        var unyOps = {
            'void': function (arg) { return void arg.resolve(); },
            'typeof': function (arg) { return typeof arg.resolve(); },
            '+': function (arg) { return +arg.resolve(); },
            '-': function (arg) { return -arg.resolve(); },
            '~': function (arg) { return ~arg.resolve(); },
            '!': function (arg) { return !arg.resolve(); }
        };

        /* resolve methods */
        ast.BinaryExpressionNode.prototype.resolve = function () {
            if (binOps[this.operator])
                return binOps[this.operator](this.left, this.right);
            return undefined;
        };
        ast.CallExpressionNode.prototype.resolve = function () {
            var callee = this.callee.resolve();
            var ctx = this.callee.object ? this.callee.object.resolve() : null;
            var args = [];
            for (var i = 0; i < this.arguments.length; i++) {
                var arg = this.arguments[i].resolve();
                args.push(arg);
            }
            return callee.apply(ctx, args);
        };
        ast.ConditionalExpressionNode.prototype.resolve = function () {
            return this.test.resolve() ? this.consequent.resolve() : this.alternate.resolve();
        };
        ast.IdentifierNode.prototype.resolve = function () {
            if ({}.hasOwnProperty.call(methods, this.name))
                return methods[this.name];
            return undefined;
        };
        ast.LiteralNode.prototype.resolve = function () {
            console.log(typeof this.value, this.value);
            return this.value;
        };
        ast.LogicalExpressionNode.prototype.resolve = function () {
            if (logOps[this.operator])
                return logOps[this.operator](this.left, this.right);
            return undefined;
        };
        ast.MemberExpressionNode.prototype.resolve = function () {
            var obj = this.object.resolve();
            if (this.property.type === 'Identifier')
                return obj[this.property.name];
            var prop = this.property.resolve();
            return obj[prop];
        };
        ast.ProgramNode.prototype.resolve = function () {
            if (this.body.resolve)
                return this.body.resolve();
            return undefined;
        };
        ast.UnaryExpressionNode.prototype.resolve = function () {
            if (unyOps[this.operator])
                return unyOps[this.operator](this.argument);
            return undefined;
        };

        return Object.freeze({
            parser: parser
        });
    }

})();