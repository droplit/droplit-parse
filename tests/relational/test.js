'use strict';

module.exports = () => {
    const config = {
        name: 'relational',
        enabled: true,
        tests: [
            {
                ast: 'ast/rel-001.ast.json',
                expression: '1 < 2',
                expected: true,
                name: 'less than operator'
            },
            {
                ast: 'ast/rel-002.ast.json',
                expression: '2 > 1',
                expected: true,
                name: 'greater than operator'
            },
            {
                ast: 'ast/rel-003.ast.json',
                expression: '1 <= 2',
                expected: true,
                name: 'less than or equal to operator'
            },
            {
                ast: 'ast/rel-004.ast.json',
                expression: '2 >= 1',
                expected: true,
                name: 'greater than or equal to operator'
            },
            {
                ast: 'ast/rel-005.ast.json',
                expression: '1 < 2 < 3',
                expected: true,
                name: 'multiple relational comparisons'
            }
        ]
    };

    Object.freeze(config);
    return config;
};