'use strict';

module.exports = () => {
    const config = {
        name: 'numeric',
        enabled: true,
        tests: [
            {
                ast: 'ast/num-001.ast.json',
                expression: '0',
                expected: 0,
                name: 'zero decimal'
            },
            {
                ast: 'ast/num-002.ast.json',
                expression: '42',
                expected: 42,
                name: 'multiple digit decimal'
            },
            {
                ast: 'ast/num-003.ast.json',
                expression: '-42',
                expected: -42,
                name: 'negative decimal digit'
            },
            {
                ast: 'ast/num-004.ast.json',
                expression: '5',
                expected: 5,
                name: 'non-zero single decimal'
            },
            // {
            //     ast: 'ast/num-005.ast.json',
            //     expression: '05',
            //     expected: 5,
            //     name: 'decimal with leading zero'
            // },
            {
                ast: 'ast/num-006.ast.json',
                expression: '.13',
                expected: 0.13,
                name: 'decimal value with fractional part'
            },
            // {
            //     ast: 'ast/num-007.ast.json',
            //     expression: '05.13',
            //     expected: 5.13,
            //     name: 'decimal value with fractional part and leading zero'
            // },
            {
                ast: 'ast/num-008.ast.json',
                expression: '1.234567e+89',
                expected: 1.234567e+89,
                name: 'e notation with positive exponent',
            },
            {
                ast: 'ast/num-009.ast.json',
                expression: '9.8765432e-10',
                expected: 9.8765432e-10,
                name: 'e notation with negative exponent'
            },
            {
                ast: 'ast/num-010.ast.json',
                expression: '0e+100',
                expected: 0e+100,
                name: 'zeroed mantissa for e notation'
            },
            {
                ast: 'ast/num-011.ast.json',
                expression: '0x0',
                expected: 0,
                name: 'zeroed hexadecimal'
            },
            {
                ast: 'ast/num-012.ast.json',
                expression: '0xabc',
                expected: 2748,
                name: 'minuscule hexadecimal'
            },
            {
                ast: 'ast/num-013.ast.json',
                expression: '0X1A',
                expected: 26,
                name: 'majuscule hexadecimal'
            },
            {
                ast: 'ast/num-014.ast.json',
                expression: '0x10',
                expected: 16,
                name: 'digital hexadecimal'
            },
            {
                ast: 'ast/num-015.ast.json',
                expression: '0X04',
                expected: 4,
                name: 'digital hexadecimal with majuscule prefix'
            },
            {
                ast: 'ast/num-016.ast.json',
                expression: '0o0',
                expected: 0,
                name: 'zero octal'
            },
            {
                ast: 'ast/num-017.ast.json',
                expression: '0O13',
                expected: 0o13,
                name: 'multiple digit octal with majuscule prefix'
            },
            {
                ast: 'ast/num-018.ast.json',
                expression: '0O013',
                expected: 0o013,
                name: 'octal with leading zero'
            },
            {
                ast: 'ast/num-019.ast.json',
                expression: '0b0',
                expected: 0,
                name: 'binary zero'
            },
            {
                ast: 'ast/num-020.ast.json',
                expression: '0B101',
                expected: 5,
                name: 'multiple digit binary with majuscule prefix'
            },
            {
                ast: 'ast/num-021.ast.json',
                expression: '0b011',
                expected: 3,
                name: 'binary with leading zero'
            }
        ]
    };

    Object.freeze(config);
    return config;
};