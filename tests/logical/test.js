'use strict';

module.exports = () => {
    const config = {
        name: 'logical',
        enabled: true,
        tests: [
            {
                ast: 'ast/log-001.ast.json',
                expression: 'false && 15',
                expected: false,
                name: 'logical AND'
            },
            {
                ast: 'ast/log-002.ast.json',
                expression: 'true && true && 15',
                expected: 15,
                name: 'multiple logical ANDs',
            },
            {
                ast: 'ast/log-003.ast.json',
                expression: 'false || 15',
                expected: 15,
                name: 'logical OR'
            },
            {
                ast: 'ast/log-004.ast.json',
                expression: 'false || true || 15',
                expected: true,
                name: 'multiple logical ORs'
            },
            {
                ast: 'ast/log-005.ast.json',
                expression: 'false || true && 15',
                expected: 15,
                name: 'logical OR and AND'
            },
            {
                ast: 'ast/log-006.ast.json',
                expression: 'false || true ^ 15',
                expected: 14,
                name: 'logical OR and XOR'
            }
        ]
    };

    Object.freeze(config);
    return config;
};