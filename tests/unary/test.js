'use strict';

module.exports = () => {
    const config = {
        name: 'unary',
        enabled: true,
        tests: [
            {
                ast: 'ast/uny-001.ast.json',
                expression: '+1',
                expected: 1,
                name: 'unary plus operator'
            },
            {
                ast: 'ast/uny-002.ast.json',
                expression: '-1',
                expected: -1,
                name: 'unary negation operator'
            },
            {
                ast: 'ast/uny-003.ast.json',
                expression: '~5',
                expected: -6,
                name: 'bitwise not operator'
            },
            {
                ast: 'ast/uny-004.ast.json',
                expression: '!true',
                expected: false,
                name: 'logical not operator'
            },
            {
                ast: 'ast/uny-005.ast.json',
                expression: 'void 1',
                expected: undefined,
                name: 'void operator'
            }
        ]
    };

    Object.freeze(config);
    return config;
};