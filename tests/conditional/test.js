'use strict';

module.exports = () => {
    const config = {
        name: 'conditional',
        enabled: true,
        tests: [
            {
                ast: 'ast/cnd-001.ast.json',
                expression: '5===5?1:2',
                expected: 1,
                name: 'conditional true'
            },
            {
                ast: 'ast/cnd-002.ast.json',
                expression: '5===1?1:2',
                expected: 2,
                name: 'conditional false',
            }
        ]
    };

    Object.freeze(config);
    return config;
};