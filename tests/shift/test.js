'use strict';

module.exports = () => {
    const config = {
        name: 'shift',
        enabled: true,
        tests: [
            {
                ast: 'ast/sft-001.ast.json',
                expression: '10 << 1',
                expected: 20,
                name: 'Left shift'
            },
            {
                ast: 'ast/sft-002.ast.json',
                expression: '10 >> 1',
                expected: 5,
                name: 'Right shift'
            },
            {
                ast: 'ast/sft-003.ast.json',
                expression: '-668991488 >>> 21',
                expected: 1729,
                name: 'Zero-filled right shift'
            }
        ]
    };

    Object.freeze(config);
    return config;
};