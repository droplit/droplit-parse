'use strict';

module.exports = () => {
    const config = {
        name: 'boolean',
        enabled: true,
        tests: [
            {
                ast: 'ast/bln-001.ast.json',
                expression: 'true',
                expected: true,
                name: 'true literal'
            },
            {
                ast: 'ast/bln-002.ast.json',
                expression: 'false',
                expected: false,
                name: 'false literal'
            }
        ]
    };

    Object.freeze(config);
    return config;
};