'use strict';

module.exports = () => {
    const config = {
        name: 'bitwise',
        enabled: true,
        tests: [
            {
                ast: 'ast/bit-001.ast.json',
                expression: '10 & 15',
                expected: 10,
                name: 'bitwise AND'
            },
            {
                ast: 'ast/bit-002.ast.json',
                expression: '10 | 15',
                expected: 15,
                name: 'bitwise OR'
            },
            {
                ast: 'ast/bit-003.ast.json',
                expression: '10 ^ 15',
                expected: 5,
                name: 'bitwise XOR'
            }
        ]
    };

    Object.freeze(config);
    return config;
};