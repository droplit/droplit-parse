'use strict';

module.exports = () => {
    const config = {
        name: 'equality',
        enabled: true,
        tests: [
            {
                ast: 'ast/equ-001.ast.json',
                expression: '5==5',
                expected: true,
                name: 'loose equality comparison'
            },
            {
                ast: 'ast/equ-002.ast.json',
                expression: '5!=5',
                expected: false,
                name: 'loose inequality comparison'
            },
            {
                ast: 'ast/equ-003.ast.json',
                expression: '5===5',
                expected: true,
                name: 'strict equality comparison'
            },
            {
                ast: 'ast/equ-004.ast.json',
                expression: '5!==5',
                expected: false,
                name: 'strict inequality comparison'
            }
        ]
    };

    Object.freeze(config);
    return config;
};