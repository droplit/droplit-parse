'use strict';

module.exports = () => {
    const config = {
        name: 'multiplicative',
        enabled: true,
        tests: [
            {
                ast: 'ast/mlt-001.ast.json',
                expression: '2*5',
                expected: 10,
                name: 'multiplication'
            },
            {
                ast: 'ast/mlt-002.ast.json',
                expression: '10/2',
                expected: 5,
                name: 'division'
            },
            {
                ast: 'ast/mlt-003.ast.json',
                expression: '15%2',
                expected: 1,
                name: 'remainder'
            },
        ]
    };

    Object.freeze(config);
    return config;
};