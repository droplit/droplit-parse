'use strict';

module.exports = () => {
    const config = {
        name: 'additive',
        enabled: true,
        tests: [
            {
                ast: 'ast/add-001.ast.json',
                expression: '4+5',
                expected: 9,
                name: 'addition'
            },
            {
                ast: 'ast/add-002.ast.json',
                expression: '5-4',
                expected: 1,
                name: 'subtraction'
            }
        ]
    };

    Object.freeze(config);
    return config;
};